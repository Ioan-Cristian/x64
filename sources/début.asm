.include "config.ent"
.include "entrées-sorties.ent"

.data
message:
	.ascii "Salut tout le monde !\n"
LONGUEUR_MESSAGE=. - message

.text
.global __début__
__début__:
	movl $DESCRIPTEUR_FICHIER_SORTIE_STANDARD, %edi
	leaq message(%rip), %rsi
	movq $LONGUEUR_MESSAGE, %rdx
	call écrire
	xorl %edi, %edi
	call sortir
