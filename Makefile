############
# Suffixes #
############

SUFFIXE_SOURCES=asm
SUFFIXE_EN_TÊTES=ent
SUFFIXE_OBJETS=obj
SUFFIXE_BINAIRES=exe

##############
# Hiérarchie #
##############

RÉPERTOIRE_DÉBOGAGE=débogage
RÉPERTOIRE_PRODUCTION=production

RÉPERTOIRE_SOURCES=sources
RÉPERTOIRE_EN_TÊTES=en-têtes

RÉPERTOIRE_OBJETS=objets
RÉPERTOIRE_OBJETS_DÉBOGAGE=$(RÉPERTOIRE_OBJETS)/$(RÉPERTOIRE_DÉBOGAGE)
RÉPERTOIRE_OBJETS_PRODUCTION=$(RÉPERTOIRE_OBJETS)/$(RÉPERTOIRE_PRODUCTION)
RÉPERTOIRES_OBJETS=$(RÉPERTOIRE_OBJETS_DÉBOGAGE) $(RÉPERTOIRE_OBJETS_PRODUCTION)

RÉPERTOIRE_BINAIRES=binaires
RÉPERTOIRE_BINAIRES_DÉBOGAGE=$(RÉPERTOIRE_BINAIRES)/$(RÉPERTOIRE_DÉBOGAGE)
RÉPERTOIRE_BINAIRES_PRODUCTION=$(RÉPERTOIRE_BINAIRES)/$(RÉPERTOIRE_PRODUCTION)
RÉPERTOIRES_BINAIRES=$(RÉPERTOIRE_BINAIRES_DÉBOGAGE) $(RÉPERTOIRE_BINAIRES_PRODUCTION)

############
# Fichiers #
############

FICHIERS_SOURCES=$(shell find $(RÉPERTOIRE_SOURCES) -name "*.$(SUFFIXE_SOURCES)")
FICHIERS_OBJET_DÉBOGAGE=\
	$(FICHIERS_SOURCES:$(RÉPERTOIRE_SOURCES)/%.$(SUFFIXE_SOURCES)=$(RÉPERTOIRE_OBJETS_DÉBOGAGE)/%.$(SUFFIXE_OBJETS))
FICHIERS_OBJET_PRODUCTION=\
	$(FICHIERS_SOURCES:$(RÉPERTOIRE_SOURCES)/%.$(SUFFIXE_SOURCES)=$(RÉPERTOIRE_OBJETS_PRODUCTION)/%.$(SUFFIXE_OBJETS))
FICHIERS_EN_TÊTES=$(shell find $(RÉPERTOIRE_EN_TÊTES) -name "*.$(SUFFIXE_EN_TÊTES)")

NOM_BASE_BINAIRE=programme_principal
NOM_BINAIRE=$(NOM_BASE_BINAIRE).$(SUFFIXE_BINAIRES)

BINAIRE_DÉBOGAGE=$(RÉPERTOIRE_BINAIRES_DÉBOGAGE)/$(NOM_BINAIRE)
BINAIRE_PRODUCTION=$(RÉPERTOIRE_BINAIRES_PRODUCTION)/$(NOM_BINAIRE)

#######################
# Outils construction #
#######################

ASSEMBLEUR=x86_64-linux-gnu-as
OPTIONS_ASSEMBLEUR_INCLUSION=-I $(RÉPERTOIRE_EN_TÊTES)
OPTIONS_ASSEMBLEUR_ERREURS=--fatal-warnings
OPTIONS_ASSEMBLEUR_BASE=\
	$(OPTIONS_ASSEMBLEUR_INCLUSION)\
	$(OPTIONS_ASSEMBLEUR_ERREURS)
OPTIONS_ASSEMBLEUR_DÉBOGAGE=\
	$(OPTIONS_ASSEMBLEUR_BASE)\
	--gen-debug --keep-locals
OPTIONS_ASSEMBLEUR_PRODUCTION=\
	$(OPTIONS_ASSEMBLEUR_BASE)

LIEUR=gold
OPTIONS_LIEUR_AVERTISSEMENTS=-fatal-warnings
OPTIONS_LIEUR_POINT_ENTRÉE=--entry __début__
OPTIONS_LIEUR_BASE=\
	$(OPTIONS_LIEUR_AVERTISSEMENTS)\
	$(OPTIONS_LIEUR_POINT_ENTRÉE)
OPTIONS_LIEUR_DÉBOGAGE=$(OPTIONS_LIEUR_BASE)
OPTIONS_LIEUR_PRODUCTION=$(OPTIONS_LIEUR_BASE) --strip-all

##########
# Règles #
##########

.PHONY: usage
usage:
	@echo "USAGE"
	@echo "initialiser: initialise le projet"
	@echo "assembler-débogage: assemble en mode débogage"
	@echo "assembler-production: assemble en mode production"
	@echo "nettoyer: supprime tous les binaires et les fichiers objet"

.PHONY: initialiser
initialiser:
	@mkdir --parents $(RÉPERTOIRES_OBJETS) $(RÉPERTOIRES_BINAIRES)

assembler-débogage: $(BINAIRE_DÉBOGAGE)

$(BINAIRE_DÉBOGAGE): $(FICHIERS_OBJET_DÉBOGAGE)
	@$(LIEUR) $(OPTIONS_LIEUR_DÉBOGAGE) $^ --output $@

$(FICHIERS_OBJET_DÉBOGAGE):\
	$(RÉPERTOIRE_OBJETS_DÉBOGAGE)/%.$(SUFFIXE_OBJETS):\
	$(RÉPERTOIRE_SOURCES)/%.$(SUFFIXE_SOURCES) $(FICHIERS_EN_TÊTES)
	@$(ASSEMBLEUR) $(OPTIONS_ASSEMBLEUR_DÉBOGAGE) $< -o $@

assembler-production: $(BINAIRE_PRODUCTION)

$(BINAIRE_PRODUCTION): $(FICHIERS_OBJET_PRODUCTION)
	@$(LIEUR) $(OPTIONS_LIEUR_PRODUCTION) $^ --output $@

$(FICHIERS_OBJET_PRODUCTION):\
	$(RÉPERTOIRE_OBJETS_PRODUCTION)/%.$(SUFFIXE_OBJETS):\
	$(RÉPERTOIRE_SOURCES)/%.$(SUFFIXE_SOURCES) $(FICHIERS_EN_TÊTES)
	@$(ASSEMBLEUR) $(OPTIONS_ASSEMBLEUR_PRODUCTION) $< -o $@

.PHONY: nettoyer
nettoyer:
	@rm -f\
		$(FICHIERS_OBJET_DÉBOGAGE)\
		$(FICHIERS_OBJET_PRODUCTION)\
		$(BINAIRE_DÉBOGAGE)\
		$(BINAIRE_PRODUCTION)
